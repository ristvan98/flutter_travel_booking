import 'package:app/models/bus.dart';
import 'package:app/screens/tour/placeScreen.dart';
import 'package:flutter/material.dart';

class ujFOglalas extends StatefulWidget {
  const ujFOglalas({Key? key}) : super(key: key);

  @override
  _ujFOglalasState createState() => _ujFOglalasState();
}

class _ujFOglalasState extends State<ujFOglalas> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView.builder(
          itemCount: BUSES.length,
          itemBuilder: (context, index){
            return Container(
              height: 80.0,
              child: Center(
                child: ListTile(
                  leading: Hero(
                    tag: BUSES[index].imgPath,
                    child: Image(image: AssetImage(BUSES[index].imgPath)),
                  ),
                  title: Text(
                    BUSES[index].utiCel,
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  subtitle: Text(
                    '${BUSES[index].feroHely} férőhely',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  trailing: InkResponse(
                    onTap: (){
                      Navigator.push(context,MaterialPageRoute(builder: (_) => PlaceScreen(bus: BUSES[index],)));
                    },
                    child: Container(
                      width: 50.0,
                      height: 50.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        color: Color(0xFFFAF6F1),
                      ),
                      child: Icon(Icons.arrow_forward,size: 25.0, color: Colors.black),
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
