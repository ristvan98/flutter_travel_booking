import 'package:app/models/bus.dart';
import 'package:app/models/foglalas.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlaceScreen extends StatefulWidget {
  final Bus bus;
  const PlaceScreen({Key? key, required this.bus}) : super(key: key);
  @override
  _PlaceScreenState createState() => _PlaceScreenState();
}

class _PlaceScreenState extends State<PlaceScreen> {
  final formKey = GlobalKey<FormState>();
  late Foglalas foglalas;

  createAlertDialog(BuildContext context){
    return showDialog(
      context: context,
      builder: (context){
      return SimpleDialog(
        contentPadding: EdgeInsets.all(15),
        title: Text('Foglalás'),
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: "Név",
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: "Hány fő?",
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 40),
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: "Felszállási hely",
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton(
                    color: Colors.blue.shade900,
                    onPressed: (){},
                    child: Text('Foglalás',style: TextStyle(color: Colors.white)),
                  ),
                  FlatButton(
                  color: Colors.blue.shade900,
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    child: Text('Mégse',style: TextStyle(color: Colors.white)),
                  ),
                ],
              )
            ],
          )
        ],
      );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Utazás"),
        backgroundColor: Colors.blue.shade900,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Hero(
                  tag: widget.bus.imgPath,
                  child: Container(
                    height: 300,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        alignment: Alignment.center,
                        image: AssetImage(widget.bus.imgPath),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              width: double.infinity,
              transform: Matrix4.translationValues(0.0, -40.0, 0.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(50.0),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.bus.utiCel,
                      style: TextStyle(
                        fontSize: 28.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.event_available,
                          size: 60.0,
                          color: Colors.black,
                        ),
                        SizedBox(width: 15.0),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Úticél ${widget.bus.utiCel}',
                              style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Text(
                              'Menetidő ${widget.bus.utIdo} óra',
                              style: TextStyle(
                                fontSize: 18.0,
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomSheet: Container(
        padding: EdgeInsets.all(20.0),
        height: 130.0,
        decoration: BoxDecoration(
          color: Color(0xFF1D4383),
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(25), topLeft: Radius.circular(25)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  '${widget.bus.feroHely}',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  'Üres férőhely',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 22.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
            FlatButton(
              padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 20.0),
              color: Color(0xFFDFF1FF),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Text(
                'Foglalás',
                style: TextStyle(
                  color: Color(0xFF309DF1),
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                createAlertDialog(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
