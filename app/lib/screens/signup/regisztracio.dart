import 'package:app/repository/ProfileRepo.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/Profile.dart';


class Regisztracio extends StatelessWidget {
  final FormKey = GlobalKey<FormState>();
  Profile p = new Profile();
  var password, confirmPassword;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Regisztráció'),
        backgroundColor: Colors.blue.shade900,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Form(
          key: FormKey,
          child: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset('img/logo.png', height: 150, width: 150,),
                  Text('Buszjegy foglaló',style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black38,fontSize: 25.0),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    margin: EdgeInsets.only(top: 50.0),
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          borderSide: BorderSide(color: Colors.transparent),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          borderSide: BorderSide(color: Colors.blue),
                        ),
                        prefixIcon: Icon(Icons.person),
                        hintText: 'Felhasználónév',
                        fillColor: Colors.grey[200],
                        filled: true,
                      ),
                      validator: (val) {
                        if(val!.isEmpty){
                          return "A felhasználónév nem lehet üres";
                        } else if(val.length < 6){
                          return "Legalább 6 karakter hosszú kell legyen";
                        }else{
                          return null;
                        }
                      },
                      onSaved: (val){
                        p.name = val;
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    margin: EdgeInsets.only(top: 10.0),
                    child: TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          borderSide: BorderSide(color: Colors.transparent),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          borderSide: BorderSide(color: Colors.blue),
                        ),
                        prefixIcon: Icon(Icons.lock),
                        hintText: 'Jelszó',
                        fillColor: Colors.grey[200],
                        filled: true,
                      ),
                      validator: (val){
                        password = val;
                      },
                      onSaved: (val){
                        p.pw = val;
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    margin: EdgeInsets.only(top: 10.0),
                    child: TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          borderSide: BorderSide(color: Colors.transparent),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          borderSide: BorderSide(color: Colors.blue),
                        ),
                        prefixIcon: Icon(Icons.lock),
                        hintText: 'Jelszó mégegyszer',
                        fillColor: Colors.grey[200],
                        filled: true,
                      ),
                      validator: (val){
                        confirmPassword = val;
                        if(val != password){
                          return "A két jelszó nem egyezik";
                        }else{
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(30.0),
                    decoration: BoxDecoration(
                        color: Colors.blue.shade900,
                        borderRadius: BorderRadius.circular(30.0)
                    ),
                    width: double.infinity,
                    child: FlatButton(
                      onPressed: () {
                        submitForm(context);
                      },
                      child: Text('Regisztrálok', style: TextStyle(color: Colors.white),),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void submitForm(BuildContext context) {
    final FormState? form = FormKey.currentState;
    if(form!.validate()){
      form.save();
    }else{
      return null;
    }
    final repo = new ProfileRepo();
    repo.Registration(p);
    ScaffoldMessenger.of(context)..removeCurrentSnackBar()..showSnackBar(SnackBar(content: Text('Sikeres Regisztrácó',style: TextStyle(color: Colors.white),), backgroundColor: Colors.green));
    Navigator.pop(context);
  }
}
