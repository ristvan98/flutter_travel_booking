import 'package:app/db/dbhelper.dart';
import 'package:sqflite/sqflite.dart';

import '../models/Profile.dart';

class ProfileRepo{
  static String PROFILE = "profile";
  late Dbhelper db;

  Future<Profile> load() async {
    Profile p = new Profile();
    var database = await db.database;
    List<Map<String, dynamic>> result = await database.query('$PROFILE',limit: 1);
    if(result.length >0){
      var data = result.first;
      p.name = data['name'];
      p.pw = data['pw'];
    }
    return p;
  }

  void Registration(Profile profil) async {
    var database = await db.database;
    String query = 'INSERT INTO $PROFILE(name,pw) VALUES(\'${profil.name}\',\'${profil.pw}\')';
    await database.transaction((txn) async {
      return await txn.rawInsert(query);
    });
  }


}