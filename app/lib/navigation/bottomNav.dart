import 'package:app/screens/profil/profil.dart';
import 'package:app/screens/tour/ujFoglalas.dart';
import 'package:flutter/material.dart';

import '../screens/tour/foglalasok.dart';
class BottomNav extends StatefulWidget{
  BottomNavState createState() => BottomNavState();
}
class BottomNavState extends State<BottomNav> {
  int currentIndex = 0;
  String _title = "Foglalások";

  List<Widget> pages = [
      Foglalasok(),
    ujFOglalas(),
      Profil(),
  ];

  void onTappedBar(int index){
    setState(() {
      currentIndex = index;
      switch(index){
        case 0 : {_title = 'Foglalások';}
        break;
        case 1 : {_title = 'Utazások';}
        break;
        case 2 : {_title = 'Profil';}
        break;
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_title),
        backgroundColor: Colors.blue.shade900,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        backgroundColor: Colors.grey.shade900,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "Foglalások"),
          BottomNavigationBarItem(icon: Icon(Icons.train), label: "Utazások"),
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "Profil"),
        ],
        onTap: onTappedBar,
      ),
      body: pages[currentIndex],
    );
  }
}


