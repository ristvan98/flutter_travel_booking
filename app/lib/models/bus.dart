class Bus{
  final String imgPath;
  final String utiCel;
  final int utIdo;
  final int feroHely;

  const Bus({
   required this.imgPath,
   required this.utiCel,
   required this.utIdo,
    required this.feroHely
});
}
const BUSES = [
  Bus(imgPath: "img/bus.jpg", utiCel: "Budapest",utIdo: 3, feroHely: 30),
  Bus(imgPath: "img/bus1.jpg", utiCel: "Budapest",utIdo: 3, feroHely: 30),
];